class Api::V1::ArticlesController < ApplicationController

  def index
  @articles = Article.all

  render json: @articles, status: :ok 
  end

  def create
    @article = Article.new(article_params)
    
    if @article.save
      render json: @article, status: :created
    else
      render json: @article.errors, status: :bad_request
    end
  end

  def show
    @article = article.find(params[:id])

    render json: @article
  end

  def update
    @article =Article.find(params[:id])

    if @article.update(article_params)
    render json: @article
    else
    render json: @article.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @article = Article.find(params[:id])

    if @article.destroy
      head :no_content, status: :ok
    else
      render json: @article.errors, status: :unprocessable_entity
    end
  end

  private
  def article_params
    params.require(:article).permit(:title)
  end
end
